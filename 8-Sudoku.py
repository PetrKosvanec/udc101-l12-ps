# THREE GOLD STARS

# Sudoku [http://en.wikipedia.org/wiki/Sudoku]
# is a logic puzzle where a game
# is defined by a partially filled
# 9 x 9 square of digits where each square
# contains one of the digits 1,2,3,4,5,6,7,8,9.
# For this question we will generalize
# and simplify the game.

# Define a procedure, check_sudoku,
# that takes as input a square list
# of lists representing an n x n
# sudoku puzzle solution and returns the boolean
# True if the input is a valid
# sudoku square and returns the boolean False
# otherwise.

# A valid sudoku square satisfies these
# two properties:

#   1. Each column of the square contains
#       each of the whole numbers from 1 to n exactly once.

#   2. Each row of the square contains each
#       of the whole numbers from 1 to n exactly once.

# You may assume the the input is square and contains at
# least one row and column.


def check_sudoku(inp):
    """ Assumes n x n (square) input.
        Returns True if input is valid sudoku, False otherwise. """

    n = len(inp)

    # digit = 1, ..., n
    digit = 1

    while digit <= n:

        # i -> row
        for i in range(n):
            row_count = 0
            column_count = 0

            # j -> column
            for j in range(n):
                if inp[i][j] == digit:
                    row_count = row_count + 1
                if inp[j][i] == digit:
                    column_count = column_count + 1
            if row_count != 1 or column_count != 1:
                return False

        digit = digit + 1

    return True





correct = [[1,2,3],
           [2,3,1],
           [3,1,2]]

incorrect = [[1,2,3,4],
             [2,3,1,3],
             [3,1,2,3],
             [4,4,4,4]]

incorrect2 = [[1,2,3,4],
              [2,3,1,4],
              [4,1,2,3],
              [3,4,1,2]]

incorrect3 = [[1,2,3,4,5],
              [2,3,1,5,6],
              [4,5,2,1,3],
              [3,4,5,2,1],
              [5,6,4,3,2]]

incorrect4 = [['a','b','c'],
              ['b','c','a'],
              ['c','a','b']]

incorrect5 = [ [1, 1.5],
               [1.5, 1]]


print check_sudoku(incorrect)
# #>>> False
#
print check_sudoku(correct)
# #>>> True
#
print check_sudoku(incorrect2)
# #>>> False
#
print check_sudoku(incorrect3)
# #>>> False
#
print check_sudoku(incorrect4)
# #>>> False
#
print check_sudoku(incorrect5)
#>>> False


